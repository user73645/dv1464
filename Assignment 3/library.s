.data
    inbuffer: .space 64
    outbuffer: .space 64
    inPos: .quad 0
    outPos: .quad 0
.text

reset:
    PUSH    %r8
    CALL    inImage
    POP     %r8
    RET

outImage:
    PUSHQ  $0                   # Stack alignment
    LEAQ   outbuffer, %rdi
    CALL   puts
    POP    %rax
    MOVQ   $0x0, outPos
    RET

inImage:
    MOVQ    $0x0, inPos         # Reset current position
    MOVQ    $inbuffer, %rdi     # fgets writes to rdi
    MOVQ    $0x40, %rsi         # max length of 64 chars
    MOVQ    stdin, %rdx         # standard in
    CALL    fgets
    RET

getInt:
    CMPQ    $0x40, inPos                  # Is the buffer full?
    JE      inImage
    LEAQ    inbuffer, %rbx                # Load buffer address
    ADD     inPos, %rbx                   # Add offset
    MOV     $0x0, %r8
    MOV     $0x0, %rax
    spaceLoop:
        MOVZB   (%rbx, %r8, 1), %rcx       # rcx = inbuffer[r8]
        INC     %r8
        INCQ    inPos
        CMP     $0x20, %rcx
        JE      spaceLoop
    CMP     $0x2B, %rcx                   # Check for '+'
    JE      positive
    CMP     $0x2D, %rcx                   # Check for '-'
    JE      negative
    JMP     unsignedPositive

    positive:
        MOVZB   (%rbx, %r8, 1), %rcx        # Skip '+' char
        INC     %r8
    unsignedPositive:
        SUB     $0x30, %rcx                 # Char -> Int
        CMP     $0x0, %rcx                  # Verify int
        JL      done
        CMP     $0x9, %rcx                  # Verify int
        JG      done
        CALL    calculate
        JMP     unsignedPositive

    negative:
        MOVZB   (%rbx, %r8, 1), %rcx        # Skip '-' char
        INC     %r8
    unsignedNegative:
        SUB     $0x30, %rcx                 # Char -> Int
        CMP     $0x0, %rcx                  # Verify int
        JL      invert
        CMP     $0x9, %rcx                  # Verify int
        JG      invert
        CALL    calculate
        JMP     unsignedNegative

    calculate:
        IMUL    $0x0A, %rax 
        ADD     %rcx, %rax
        MOVZB   (%rbx, %r8, 1), %rcx        # Feed next number
        INC     %r8
        INCQ    inPos
        RET

    invert:
        NEG     %rax

    done:
        RET

getChar:
    LEAQ    inPos, %r8              # Fetch inPos
    MOV     (%r8), %r8              # Dereference r8

    CMP     $0x0, %r8               # inbuffer empty?
    CALL    reset                   # Read more data

    CMP     $0x40, %r8              # inbuffer full?
    CALL    reset                   # Read more data

    LEAQ    inbuffer, %rbx          # Fetch inbuffer
    MOVZB   (%rbx, %r8, 1), %rax    # Read char rcx = inbuffer[r8]
    INCQ    inPos                   # Increment current position in inbuffer
    RET


getOutPos:
    LEAQ    outPos, %r8
    MOV     (%r8), %rax       # Dereference, and return to rax
    RET

getInPos:
    LEAQ    inPos, %r8
    MOV     (%r8), %rax       # Dereference, and return to rax
    RET
    
setInPos:
    MOV     %rdi, inPos
    RET

setOutPos:
    MOV     %rdi, outPos
    RET

putChar:
    MOV     outPos, %r8
    CMPQ    $0x40, outPos          # Outbuffer full
    JNE     putChar2
    PUSH    %rdi
    CALL    outImage
    POP     %rdi
    putChar2:
    LEAQ    outbuffer, %rbx
    ADD     %r8, %rbx           # Add offset
    MOV     %rdi, (%rbx)        # put char in buffer
    INCQ    outPos
    RET 

putInt:
    CMP   $0x0, %rdi
    JGE   putInt2              # Positive or 0?

    PUSH    %rdi                # Backup parameter
    MOV     $0x2D, %rdi         # Insert '-'
    CALL    putChar
    POP     %rdi                # Restore parameter
    NEG     %rdi                # Invert number

    putInt2:
    MOV    $0x1, %r8            # integer length
    MOV    $0x0A, %rbx          # Divider
    MOV    %rdi, %rax           # Dividend and quotient

    division:
        CQTO                    # Sign extension
        IDIV    %rbx
        IMUL    $0x0A, %r8
        CMP     $0x0A, %rax     
        JGE     division        # Run while quotient > 10
    CMP     %rdi, %rdx
    JE      putInt3

    IMUL    %rax, %r8
    SUB     %r8, %rdi
    ADD     $0x30, %rax           # int to char

    PUSH    %rdi
    MOV     %rax, %rdi
    CALL    putChar
    POP     %rdi
    CMP     $0x0A, %rdi
    JGE     putInt2
    putInt3:
    MOV     %rdx, %rdi
    ADD     $0x30, %rdi           # int to char
    CALL    putChar
    RET


getText:
    LEA     inbuffer, %rbx     # Fetch inbuffer
    MOV     $0x0, %rax         # set char-counter to 0
    MOV     inPos, %r8         # Fetch inPos
    CMPQ    $0x0, inPos        # buf empty?
    JNE     getText2

    PUSH     %rdi              # Save parameters
    PUSH     %rsi
    CALL     inImage           # Read data
    POP      %rsi
    POP      %rdi

    getText2:
        MOVZB   (%rbx, %r8, 1), %r10       # r10 = buf[r8]
        MOV     %r10, (%rdi)               # Write current index to buf
        INC     %rdi

        INC     %r8                        # Increment char array index
        INC     %rax                       # Increment char counter
        INCQ    inPos                      # Increment position in inbuffer

        DEC     %rsi                       # Decrement expected chars remaining
        CMP     $0x0, %rsi                 # Check if all chars are transfered
        JNE     getText2
    RET


putText:
    LEA     outbuffer, %rbx      # Get outbuffer address
    ADD     outPos, %rbx         # Add offset
    DECQ    outPos               # Prepare for early increment

    CMPQ    $0x40, outPos        # Is outbuffer full?
    JNE     putText2
    PUSH    %rdi                 # Save parameter
    CALL    outImage
    POP     %rdi
    JMP     putText

    putText2:       
        MOVZB   (%rdi), %r8
        INC     %rdi
        MOV     %r8, (%rbx)
        INC     %rbx
        INCQ    outPos
        CMP     $0x0, %r8
        JG      putText2
    RET
