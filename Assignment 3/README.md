# Compiling and running
1. To compile with gcc, run the following command:

    `gcc -no-pie -fPIC *.s -o a.out`

2. If one wishes to step through the program using a debugger such as gdb, add the -gddb flag like so:

    `gcc -ggdb -no-pie -fPIC *.s -o a.out`

3. The program can then be run normally by running the executable like so:

    `./a.out`
