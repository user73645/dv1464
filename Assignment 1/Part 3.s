.data


.equ SWI_PrInt,0x6b
.equ Stdout, 1
.equ SWI_Exit, 0x11


num: .word 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0
newline: .asciz "\n"


.text
.global main

main:
	MOV r3, #0	@ i = 0
	LDR r2, =num	@ r2 = *num

	again:
		MOV r1, #1		@ result = 1
		LDR r0, [r2, r3]	@ for r0 in r2: r3++
		MOV r4, r0
		CMP r0, #0		@ Stop at 0
		BEQ done
		BL factorial		@ factorial(r0, r
		return:			@ continue here
		ADD r3, r3, #4		@ Next index

		MOV r0, #Stdout
		SWI SWI_PrInt

		LDR r0, =newline
		SWI 0x02

		BAL again

factorial:
	CMP r4, #0		@ All numbers miltiplied?
	  BEQ return		@ exit condition
	MUL r1, r4, r1		@ Multiply by number
	SUB r4, r4, #1		@ Decrement number
	BL factorial		@ Recurse?

done:
	SWI SWI_Exit
