.data

.equ SWI_PrInt,0x6b  @ Write integer
.equ Stdout, 1
.equ SWI_Exit, 0x11  @ Stop execution

test:
	.word	1, 3, 5, 7, 9, 8, 6, 4, 2, 0

textA: .asciz "Lab1, Assignment 2\n"
textB: .asciz "\n"
textC: .asciz "Done\n"

.text
.global main

main:
	STMDB sp!, {lr}

	LDR r0, =textA		@ Print "Lab1, Assignmen 2"
	SWI 0x02

	LDR r0, =test		@ r0 = *test
	mov r3, #0		@ r4 = 0
	mov r2, #1		@ Set r2 as minimum element
	BL findMax		@ findMax()

	MOV r0, #Stdout		@ print
	MOV r1, r2		@ the
	SWI SWI_PrInt		@ integer

	LDR r0, =textB		@ Print "\n"
	SWI 0x02
	LDR r0, =textC		@ Print "Done"
	SWI 0x02

	LDMIA sp!, {pc}
	SWI 0			@ Exit

findMax:
	again:
		LDR r1, [r0, r3]	@ Start at index 0
		CMP r1, #0		@ Array is 0-terminated
		BEQ done
		CMP r1, r2		@ If index == max elem 
		MOVGT r2, r1		@ 
		MOVNE r2, r2		@ 
		ADD r3, r3, #4		@ Next index
		BAL again 		@ else continue

done:
	MOV r0, r2
	BX lr 
.end
